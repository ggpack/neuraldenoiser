# Denoiser

[![Pipeline status](https://gitlab.com/ggpack/neuraldenoiser/badges/master/pipeline.svg)](https://gitlab.com/ggpack/neuraldenoiser/pipelines)
[![Coverage report](https://gitlab.com/ggpack/neuraldenoiser/badges/master/coverage.svg?job=unittest)](https://gitlab.com/ggpack/neuraldenoiser/-/jobs)

[![License](https://img.shields.io/badge/license-Apache2-blue.svg)](https://gitlab.com/ggpack/neuraldenoiser/-/blob/master/LICENSE)


Leverage the power of neural networks to remove the noise from recordings or radio 🧠🎶


## Pass it some audio, get it cleaned up

- a poor recording
- a live stream (car radio)

The network is trained with a variety of records patched with a variety of types of noises.


# Development

## Dependencies

* python3
* pip
* virtualenv

## Installation

``` bash
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

# Testing

Ran on each push, see the dedicated folder [test](https://gitlab.com/ggpack/neuraldenoiser/-/blob/master/test).


# Thanks
Icons made by [pongsakornRed](https://www.flaticon.com/authors/pongsakornred) from [www.flaticon.com](https://www.flaticon.com)
