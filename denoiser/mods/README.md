# Modules
They represent the main command to execute in the program.
They have been designed as `independant plugins` to the main program and are automatically registered if they comply to the schema defined in `utils`.

## Inputs / outputs

## Options subparsers

If needed, a mod can implement the function `registerArgparser` that will be called during the main argparser creation. It will provide the mod specific options to the CLI.
This way, the options and their usage are co-located.
