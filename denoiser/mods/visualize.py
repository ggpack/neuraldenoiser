"""
Module used to debug the input data.
"""
import logging
import matplotlib.pyplot as plt

from audioAdaptors import WavAdaptor

from .manager import Strategy


class Visualize(Strategy):

	@staticmethod
	def execute(ctx):
		rate, data = WavAdaptor.read(ctx.args.input)

		#print(data)
		plt.plot(data[1000:])
		plt.show()

	@staticmethod
	def registerArgparser(iModName, iModParsers):
		aParser = iModParsers.add_parser(iModName,             help = "Display the input for debug purpose")
		aParser.add_argument("-i", "--input", required = True, help = "The input")
