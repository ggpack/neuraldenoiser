import logging
import matplotlib.pyplot as plt

from audioAdaptors import WavAdaptor
from .manager import Strategy
from models import Signal

def generateTrainingData(signal, trainingSetSize):
	trainingSet = []
	for idx in range(trainingSetSize):
		copy = signal.duplicate()
		copy.addNormalNoise(0.05).addHoles(0.01, 5, 40)
		trainingSet.append(copy.list)

	return trainingSet

class Train(Strategy):

	@staticmethod
	def execute(ctx):
		rate, data = WavAdaptor.read(ctx.args.input)

		ref = data[:10000] # list conversion

		sig = Signal(ref)
		sig.fastNormalize()

		trainingSet = generateTrainingData(sig, 100)

		#plt.plot(sig.list)
		# plt.plot(trainingSet[-1])
		#splt.show()


	@staticmethod
	def registerArgparser(iModName, iModParsers):
		aParser = iModParsers.add_parser(iModName,             help = "Display the input for debug purpose")
		aParser.add_argument("-i", "--input", required = True, help = "The input")

