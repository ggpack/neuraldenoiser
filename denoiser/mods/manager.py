
class Register:
	"""
	Utility to register a mod and retrieve it.
	"""

	def getAll():
		registeredMods = Strategy.__subclasses__()
		return {cls.__name__.lower(): cls for cls in registeredMods}

	def get(iKey):
		"""
		Gives one registered strategy instance or the default parent class.
		"""
		return Register.getAll().get(iKey, None)

	def registerSubparsers(ioMainParser):
		"""
		Adds the sub command line parsers as defined by the mod with the optional function registerArgparser.
		"""
		aModparsers = ioMainParser.add_subparsers(dest = "mod", help = "The strategy")
		aModparsers.required = True

		for aModName, aMod in Register.getAll().items():
			aMod.registerArgparser(aModName, aModparsers)


class Strategy:
	"""
	Base class to define a generic module stategy API.
	"""
	@staticmethod
	def execute(iArgs, oProgBom):
		pass

	@staticmethod
	def registerArgparser(iModName, iModParsers):
		pass
