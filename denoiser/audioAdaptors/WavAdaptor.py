import scipy.io.wavfile
import logging

def read(filepath, normalized = False):
	return scipy.io.wavfile.read(filepath)
