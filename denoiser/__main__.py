#! /usr/bin/env python3

import argparse
import argcomplete
import logchain
import os

from types import SimpleNamespace

import mods

def main(iArgs):

	chainer = logchain.LogChainer(verbosity = iArgs.verbose, name = "denoiser")
	logger = chainer.initLogging()
	logger.info(iArgs)

	aContext = SimpleNamespace(args = iArgs, returnVal = 0)

	try:
		aStrat = mods.manager.Register.get(iArgs.mod)
		aStrat.execute(aContext)

	except:
		logger.exception("Issue in main")
		aContext.returnVal = 1

	logger.info("Done")

	return aContext.returnVal


def getArgParser():
	aParser = argparse.ArgumentParser(description = "Denoiser", prog = "denoiser")

	aParser.add_argument("-v", "--verbose", action = "count", default = 2)
	aParser.add_argument("-V", "--version", action = "version", version = "%(prog)s version 0.0.1", help = "Outputs the software version in the format \d+.\d+.\d+")

	mods.manager.Register.registerSubparsers(aParser)

	# to source: eval "$(register-python-argcomplete denoise)"
	argcomplete.autocomplete(aParser)

	return aParser


if __name__ == "__main__":
	exit( main(getArgParser().parse_args()) )
