import random
import logging

import numpy as np

def iterateChunks(cnt, nbChunk, cb):
	length = len(cnt)

	# If it does not perfectly fit, go beyond with a last partial chunk.
	chunkSize = length // nbChunk + 1 if length % nbChunk else length // nbChunk

	for chunkIdx in range(nbChunk):
		startIdx = chunkIdx * chunkSize
		endIdx = min(length, startIdx + chunkSize)
		if startIdx >= endIdx:
			break

		#print("[%s .. %s [" % (startIdx, endIdx))

		chunk = cnt[startIdx : endIdx]
		cb(chunk)


class Signal:

	def __init__(self, list):
		self.list = list


	def fastNormalize(self):

		def clamp(value, mini, maxi):
			return max(min(value, maxi), mini)

		def symClamp(value, ampli):
			return clamp(value, -1 * ampli, ampli) / ampli

		mini, maxi = self.getEnveloppe()
		#print(mini, maxi)

		offset = (maxi + mini) / 2
		# Exagerate the ampli because maxi is not the absolute max
		ampli  = 1.01 * (maxi - mini) / 2

		self.list = [symClamp(e - offset, ampli) for e in self.list]


	def getEnveloppe(self):
		minis = []
		maxis = []

		def getExtremes(chunk):
			minis.append(min(chunk))
			maxis.append(max(chunk))

		iterateChunks(self.list, 10, getExtremes)

		return np.median(minis), np.median(maxis)

	def duplicate(self):
		return Signal(self.list[:])

	def addNormalNoise(self, relativeStrength):
		"""
		Signal is a list of pairs (left, right), the audio tracks
		"""
		ampliMax = 1
		ampliNoise = ampliMax * relativeStrength

		result = []

		for val in self.list:
			#result.append([random.normalvariate(val[0], ampliNoise), random.normalvariate(val[1], ampliNoise)])
			result.append([random.normalvariate(val, ampliNoise)])

		self.list = result
		return self


	def addHoles(self, probability, minDuration, maxDuration):
		"""
		From times to times, deletes the signal
		"""
		result = []
		inHoleIndex = 0
		holeDuration = 0

		length = len(self.list)

		for idx in range(length):
			if inHoleIndex < holeDuration:
				inHoleIndex += 1
			else:
				if random.random() > probability:
					result += self.list[idx]
				else:
					inHoleIndex = 1
					holeDuration = min(int(random.uniform(minDuration, maxDuration)), length - idx)
					result += [0] * holeDuration

		self.list = result
		return self
