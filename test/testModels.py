import unittest
import math
from unittest.mock import patch
from types import SimpleNamespace as SN

from denoiser import models


def generateSin(stop, length):
	return [math.sin(idx * stop / length) for idx in range(length)]


class TestModels(unittest.TestCase):

	def test_iterateChunks(self):
		result = []
		nbChunk = 19
		cnt = list(range(20))
		nbChunksDone = []

		def cb(chunk):
			nbChunksDone.append(1)
			for e in chunk:
				result.append(e)

		models.iterateChunks(cnt, nbChunk, cb)
		# The algo could manage it iterate in only 10 chunks
		self.assertEqual(sum(nbChunksDone), 10)
		self.assertEqual(result, cnt)

	def test_getEnveloppe(self):
		sigSize = 12
		sig = models.Signal(range(sigSize))
		mini, maxi = sig.getEnveloppe()
		self.assertEqual((mini, maxi), (5, 6))

		sigSize = 1000
		sig = models.Signal(generateSin(45, sigSize))
		mini, maxi = sig.getEnveloppe()

		self.assertTrue(0.999 < maxi < 1)
		self.assertTrue(-0.999 > mini > -1)

		# Resilience in case of signal pic
		sig.list[sigSize//2] = 1000
		sig.list[sigSize//3] = -1000

		mini, maxi = sig.getEnveloppe()
		# The median enveloppe was properly fitted
		self.assertTrue(0.999 < maxi < 1)
		self.assertTrue(-0.999 > mini > -1)

	def test_fastNormalize(self):
		sigSize = 1000
		sig = models.Signal(generateSin(45, sigSize))
		sig.list[sigSize//2] = 1000
		sig.list[sigSize//3] = -1000
		sig.fastNormalize()

		# Extreme val due to the pic
		self.assertEqual(max(sig.list), 1)
		self.assertEqual(min(sig.list), -1)
