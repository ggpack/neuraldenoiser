"""
Command line interface
"""
import unittest
import io
from contextlib import redirect_stdout, redirect_stderr
from types import SimpleNamespace

from denoiser import __main__ as main

# class MockMod(modManager.ModStrategy):
# 	@staticmethod
# 	def execute(ctx):
# 		ctx.returnVal = 4444


class TestCli(unittest.TestCase):

	def test_reject_and_usual_option(self):

		aArgParser = main.getArgParser()

		aMockOut = io.StringIO()
		with redirect_stderr(aMockOut):
			with self.assertRaises(SystemExit) as ctxManager:
				aArgParser.parse_args(["invalidArg"])
			# Return KO
			self.assertEqual(ctxManager.exception.code, 2)
		self.assertTrue(aMockOut.getvalue().startswith("usage"))

		aMockOut = io.StringIO()
		with redirect_stdout(aMockOut):
			with self.assertRaises(SystemExit) as ctxManager:
				aArgParser.parse_args(["--help"])
			# Return OK
			self.assertEqual(ctxManager.exception.code, 0)
		self.assertTrue(aMockOut.getvalue().startswith("usage"))

		aMockOut = io.StringIO()
		with redirect_stdout(aMockOut):
			with self.assertRaises(SystemExit) as ctxManager:
				aArgParser.parse_args(["--version"])
			# Return OK
			self.assertEqual(ctxManager.exception.code, 0)
		self.assertTrue(aMockOut.getvalue().startswith("denoiser version"))


	# def test_mod_arg_registration(self):

	# 	aArgParser = main.getArgParser()
	# 	args = aArgParser.parse_args(["visualize", "-h"])
	# 	self.assertEqual(args.mod, "visualize")


	# def test_mod_execution(self):

	# 	args = SimpleNamespace(verbose = 0, mod = "mockmod")
	# 	returnVal = main.main(args)
	# 	self.assertEqual(returnVal, 4445)
