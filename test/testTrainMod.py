import unittest
import math
from unittest.mock import patch
from types import SimpleNamespace as SN

from denoiser.mods import train
from denoiser.models import *

#from audioAdaptors import WavAdaptor

#import matplotlib.pyplot as plt
#plt.plot(sig.list)
#plt.show()

def generateSin(stop, length):
	return [math.sin(idx * stop / length) for idx in range(length)]


class TestTrainMod(unittest.TestCase):

	@patch("audioAdaptors.WavAdaptor.read")
	def test_execute(self, mock_read):
		mock_read.return_value = 1, range(1000)
		aCtx = SN(args = SN(input = 4))
		train.Train.execute(aCtx)

	def test_generateTrainingData(self):
		sigSize = 1000
		tsSize = 20
		sig = Signal(range(sigSize))
		trainingSet = train.generateTrainingData(sig, tsSize)
		self.assertEqual(len(trainingSet), tsSize)

		for item in trainingSet:
			self.assertEqual(len(item), sigSize)

	def test_iterateChunks(self):
		result = []
		nbChunk = 19
		cnt = list(range(20))
		nbChunksDone = []

		def cb(chunk):
			nbChunksDone.append(1)
			for e in chunk:
				result.append(e)

		iterateChunks(cnt, nbChunk, cb)
		# The algo could manage it iterate in only 10 chunks
		self.assertEqual(sum(nbChunksDone), 10)
		self.assertEqual(result, cnt)

	def test_getEnveloppe(self):
		sigSize = 12
		sig = Signal(range(sigSize))
		mini, maxi = sig.getEnveloppe()
		self.assertEqual((mini, maxi), (5, 6))

		sigSize = 1000
		sig = Signal(generateSin(45, sigSize))
		mini, maxi = sig.getEnveloppe()

		self.assertTrue(0.999 < maxi < 1)
		self.assertTrue(-0.999 > mini > -1)

		# Resilience in case of signal pic
		sig.list[sigSize//2] = 1000
		sig.list[sigSize//3] = -1000

		mini, maxi = sig.getEnveloppe()
		# The median enveloppe was properly fitted
		self.assertTrue(0.999 < maxi < 1)
		self.assertTrue(-0.999 > mini > -1)

	def test_fastNormalize(self):
		sigSize = 1000
		sig = Signal(generateSin(45, sigSize))
		sig.list[sigSize//2] = 1000
		sig.list[sigSize//3] = -1000
		sig.fastNormalize()

		# Extreme val due to the pic
		self.assertEqual(max(sig.list), 1)
		self.assertEqual(min(sig.list), -1)
