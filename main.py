
import matplotlib.pyplot as plt
import scipy.io.wavfile
import tkinter

import random

rate, data = scipy.io.wavfile.read("test/data/audioInputSamples/pure_152.wav")
data = data[:2000]
print(rate, data[0])
plt.plot(data[:1000])


class Signal:

	def __init__(self, list):
		self.list = list

	def addNormalNoise(self, relativeStrength):
		"""
		Signal is a list of pairs (left, right), the audio tracks
		"""
		ampliMax = 26000
		ampliNoise = ampliMax * relativeStrength

		result = []

		for val in self.list:
			#result.append([random.normalvariate(val[0], ampliNoise), random.normalvariate(val[1], ampliNoise)])
			result.append([random.normalvariate(val, ampliNoise)])

		self.list = result
		return self


	def addHoles(self, probability, minDuration, maxDuration):
		"""
		From times to times, deletes the signal
		"""
		result = []
		inHoleIndex = 0
		holeDuration = 0

		length = len(self.list)


		print(self.list[:100])

		for idx in range(length):
			if inHoleIndex < holeDuration:
				inHoleIndex += 1
			else:
				if random.random() > probability:
					result += self.list[idx]
				else:
					inHoleIndex = 1
					holeDuration = min(int(random.uniform(minDuration, maxDuration)), length - idx)
					result += [0] * holeDuration

		self.list = result
		return self

sig = Signal(data)
sig.addHoles(0.01, 5, 40).addNormalNoise(0.05)


plt.plot(sig.list[:1000])
plt.show()





